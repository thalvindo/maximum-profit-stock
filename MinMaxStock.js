let prices = [100, 180, 30, 310, 40, 535, 695, 1023];
let profits = Array();
let profitsTimerHashMap = new Map()

const maxProfit = (price) => {
    let n = price.length;
    let temporaryAnswer = Array();
    
    if(n == 1) {
        return;
    }
    let i = 0;
    while(i< n-1) {
        while((i < n-1) && (price[i + 1] <= price[i]) ) {
            i++;
        }

        if(i == n-1)
            break;

        let buy = i;
        i++;

        while ((i < n) && (price[i] >= price[i - 1])){
            i++;
        }

        let sell = i - 1;

        temporaryAnswer.push([buy, sell, price[sell]-price[buy]]);
        profitsTimerHashMap.set(price[sell]-price[buy], [buy, sell]);
    }

    return temporaryAnswer;
}

const handleMap = (val) => {
    profits.push(val[2]);
}

const temporaryAnswer = maxProfit(prices);

temporaryAnswer.map(handleMap);

const maximumProfit = Math.max(...profits);
const answer = profitsTimerHashMap.get(Math.max(maximumProfit));

console.log('buy at day: ', answer[0]);
console.log('sell at day: ', answer[1]);
console.log('profit: ', Math.max(maximumProfit));